# Template to start a Lando project

With xDebug, in Visual Code

## To serve

1. Start Lando en composer install

   ```sh
   lando start
   cd docroot && composer i
   ```

2. F5 &rarr; Listen for XDebug

![index.php with centered div](https://i.imgur.com/apHa4CJ.png)

### Sources:

- https://docs.lando.dev/config/env.html
- https://xdebug.org/wizard
- https://docs.lando.dev/guides/lando-with-vscode.html
